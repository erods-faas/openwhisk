/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package whisk.core.containerpool.logging

import akka.actor.ActorSystem
import whisk.common.TransactionId
import whisk.core.containerpool.Container
import whisk.core.database.UserContext
import whisk.core.entity.{ActivationLogs, ExecutableWhiskAction, Identity, WhiskActivation}

import scala.concurrent.Future

object NullLogStore extends LogStore {

  override def containerParameters: Map[String, Set[String]] = Map("--log-driver" -> Set("none"))

  override def collectLogs(transid: TransactionId,
                           user: Identity,
                           activation: WhiskActivation,
                           container: Container,
                           action: ExecutableWhiskAction): Future[ActivationLogs] =
    Future.successful(ActivationLogs())

  override def fetchLogs(activation: WhiskActivation, context: UserContext): Future[ActivationLogs] =
    Future.successful(ActivationLogs())
}

object NullLogStoreProvider extends LogStoreProvider {
  override def instance(actorSystem: ActorSystem): LogStore = NullLogStore
}
