package whisk.common

import pureconfig._
import com.oracle.jrockit.jfr._

case class JfrTracingConfig(enabled: Boolean)

trait JfrTracer {

  def traceControllerHttpRequest()(implicit transid: TransactionId): Unit = {}
  def traceControllerHttpResponse()(implicit transid: TransactionId): Unit = {}
  def traceControllerKafkaSend()(implicit transid: TransactionId): Unit = {}
  def traceControllerKafkaReceive()(implicit transid: TransactionId): Unit = {}
  def traceInvokerKafkaReceive()(implicit transid: TransactionId): Unit = {}
  def traceInvokerKafkaSend()(implicit transid: TransactionId): Unit = {}
  def traceInvokerDockerRequest()(implicit transid: TransactionId): Unit = {}
  def traceInvokerDockerResponse()(implicit transid: TransactionId): Unit = {}
}

case object JfrTracing {

  val config = loadConfigOrThrow[JfrTracingConfig]("whisk.jfrtracing")

  val tracer: JfrTracer = createTracer(config)

  private def createTracer(config: JfrTracingConfig) = {
    if (config.enabled) {
      new EnabledJfrTracer(config);
    }
    else {
      new DisabledJfrTracer();
    }
  }
}

class DisabledJfrTracer extends JfrTracer {}

class EnabledJfrTracer(val config: JfrTracingConfig) extends JfrTracer {

  private val producer = new Producer("OpenWhisk Tracing", "", "http://dummy.org/openwhisk/tracing")

  private val transid = new DynamicValue(
    "transid",
    "Transaction ID",
    "Identifier of the transaction",
    ContentType.None,
    classOf[String])

  private def createEvent(path: String, desc: String) = {
    producer.createDynamicInstantEvent(
      path, desc, path,
      true, false,
      transid)
  }

  private val controllerHttpRequestFactory = createEvent(
    "whisk/controller/http/request",
    "The controller just received the HTTP request from the client")  

  private val controllerHttpResponseFactory = createEvent(
    "whisk/controller/http/response",
    "The controller is about to send the HTTP response to the client")

  private val controllerKafkaSendFactory = createEvent(
    "whisk/controller/kafka/send",
    "The controller is about to send the request to the Invoker through Kafka")

  private val controllerKafkaReceiveFactory = createEvent(
    "whisk/controller/kafka/receive",
    "The controller just received the response from the Invoker through Kafka")

  private val invokerKafkaReceiveFactory = createEvent(
    "whisk/invoker/kafka/receive",
    "The Invoker just received the request from the Controller through Kafka")

  private val invokerKafkaSendFactory = createEvent(
    "whisk/invoker/kafka/send",
    "The Invoker is about to send the response to the Controller through Kafka")

  private val invokerDockerRequestFactory = createEvent(
    "whisk/invoker/docker/request",
    "The Invoker is about to send the request to the Docker container")

  private val invokerDockerResponseFactory = createEvent(
    "whisk/invoker/docker/response",
    "The Invoker just received the response from the Docker container")

  producer.register()

  private val controllerHttpRequest = new ThreadLocal[InstantEvent]{
    override def initialValue = controllerHttpRequestFactory.newInstantEvent()
  }
  private val controllerHttpResponse = new ThreadLocal[InstantEvent]{
    override def initialValue = controllerHttpResponseFactory.newInstantEvent()
  }
  private val controllerKafkaSend = new ThreadLocal[InstantEvent]{
    override def initialValue = controllerKafkaSendFactory.newInstantEvent()
  }
  private val controllerKafkaReceive = new ThreadLocal[InstantEvent]{
    override def initialValue = controllerKafkaReceiveFactory.newInstantEvent()
  }
  private val invokerKafkaReceive = new ThreadLocal[InstantEvent]{
    override def initialValue = invokerKafkaReceiveFactory.newInstantEvent()
  }
  private val invokerKafkaSend = new ThreadLocal[InstantEvent]{
    override def initialValue = invokerKafkaSendFactory.newInstantEvent()
  }
  private val invokerDockerRequest = new ThreadLocal[InstantEvent]{
    override def initialValue = invokerDockerRequestFactory.newInstantEvent()
  }
  private val invokerDockerResponse = new ThreadLocal[InstantEvent]{
    override def initialValue = invokerDockerResponseFactory.newInstantEvent()
  }

  override def traceControllerHttpRequest()(implicit transid: TransactionId) = {
    val event = controllerHttpRequest.get()
    event.setValue("transid", transid.id)
    event.commit()
  }

  override def traceControllerHttpResponse()(implicit transid: TransactionId) = {
    val event = controllerHttpResponse.get()
    event.setValue("transid", transid.id)
    event.commit()
  }

  override def traceControllerKafkaSend()(implicit transid: TransactionId) = {
    val event = controllerKafkaSend.get()
    event.setValue("transid", transid.id)
    event.commit()
  }

  override def traceControllerKafkaReceive()(implicit transid: TransactionId) = {
    val event = controllerKafkaReceive.get()
    event.setValue("transid", transid.id)
    event.commit()
  }

  override def traceInvokerKafkaReceive()(implicit transid: TransactionId) = {
    val event = invokerKafkaReceive.get()
    event.setValue("transid", transid.id)
    event.commit()
  }

  override def traceInvokerKafkaSend()(implicit transid: TransactionId) = {
    val event = invokerKafkaSend.get()
    event.setValue("transid", transid.id)
    event.commit()
  }

  override def traceInvokerDockerRequest()(implicit transid: TransactionId) = {
    val event = invokerDockerRequest.get()
    event.setValue("transid", transid.id)
    event.commit()
  }

  override def traceInvokerDockerResponse()(implicit transid: TransactionId) = {
    val event = invokerDockerResponse.get()
    event.setValue("transid", transid.id)
    event.commit()
  }
}