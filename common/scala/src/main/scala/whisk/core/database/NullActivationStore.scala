/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package whisk.core.database

import java.time.Instant

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import spray.json.JsObject
import whisk.common.{Logging, TransactionId}
import whisk.core.entity.{ActivationId, DocInfo, EntityPath, WhiskActivation}

import scala.concurrent.Future

object NullActivationStore extends ActivationStore {

  def store(activation: WhiskActivation, context: UserContext)(
    implicit transid: TransactionId,
    notifier: Option[CacheChangeNotification]): Future[DocInfo] = {

    Future.successful(null)
  }

  def get(activationId: ActivationId, context: UserContext)(
    implicit transid: TransactionId): Future[WhiskActivation] = {
    Future.failed(new UnsupportedOperationException())
  }

  /**
   * Here there is added overhead of retrieving the specified activation before deleting it, so this method should not
   * be used in production or performance related code.
   */
  def delete(activationId: ActivationId, context: UserContext)(
      implicit transid: TransactionId,
      notifier: Option[CacheChangeNotification]): Future[Boolean] = {
    Future.failed(new UnsupportedOperationException())
  }

  def countActivationsInNamespace(namespace: EntityPath,
      name: Option[EntityPath] = None,
      skip: Int,
      since: Option[Instant] = None,
      upto: Option[Instant] = None,
      context: UserContext)(implicit transid: TransactionId): Future[JsObject] = {
    Future.failed(new UnsupportedOperationException())
  }

  def listActivationsMatchingName(
      namespace: EntityPath,
      name: EntityPath,
      skip: Int,
      limit: Int,
      includeDocs: Boolean = false,
      since: Option[Instant] = None,
      upto: Option[Instant] = None,
      context: UserContext)(implicit transid: TransactionId): Future[Either[List[JsObject], List[WhiskActivation]]] = {
    Future.failed(new UnsupportedOperationException())
  }

  def listActivationsInNamespace(
      namespace: EntityPath,
      skip: Int,
      limit: Int,
      includeDocs: Boolean = false,
      since: Option[Instant] = None,
      upto: Option[Instant] = None,
      context: UserContext)(implicit transid: TransactionId): Future[Either[List[JsObject], List[WhiskActivation]]] = {
    Future.failed(new UnsupportedOperationException())
  }
}

object NullActivationStoreProvider extends ActivationStoreProvider {
  override def instance(actorSystem: ActorSystem, actorMaterializer: ActorMaterializer, logging: Logging) =
    NullActivationStore
}
